import netaddr
import os

class AppConfig(object):
    ENABLE_SUBDOMAINS = True
    SERVER_NAME = os.environ['SERVER_NAME']
    SQLALCHEMY_DATABASE_URI = os.environ['SQLALCHEMY_DATABASE_URI']
    ADMIN_IPS = netaddr.ip.IPV4_PRIVATE
