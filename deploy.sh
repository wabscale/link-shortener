#!/bin/bash

cd $(dirname $(realpath $0))

if [ -f .env ]; then
    export $(cat .env | xargs)
fi

docker-compose up -d --force-recreate
