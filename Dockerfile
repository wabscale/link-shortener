FROM python:3.8-alpine

WORKDIR /opt/app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt && apk add binutils libc-dev
COPY . .

CMD gunicorn -b 0.0.0.0:5000 -w 4 app:app
